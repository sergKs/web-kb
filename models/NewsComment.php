<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_comments".
 *
 * @property int $id
 * @property string $text
 * @property string $createdAt
 * @property string $updatedAt
 * @property int $newsId
 *
 * @property News $news
 */
class NewsComment extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'news_comments';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['text', 'createdAt', 'updatedAt', 'newsId'], 'required'],
			[['text'], 'string'],
			[['createdAt', 'updatedAt'], 'safe'],
			[['newsId'], 'integer'],
			[['newsId'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['newsId' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'text' => 'Text',
			'createdAt' => 'Created At',
			'updatedAt' => 'Updated At',
			'newsId' => 'News ID',
		];
	}

	/**
	 * Gets query for [[News]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getNews()
	{
		return $this->hasOne(News::className(), ['id' => 'newsId']);
	}
}
