<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $createdAt
 * @property string|null $updatedAt
 * @property int $categoryId
 *
 * @property NewsComment[] $newsComments
 */
class News extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'news';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['title', 'createdAt'], 'required'],
			[['createdAt', 'updatedAt'], 'safe'],
			[['title'], 'string', 'max' => 128],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
			'createdAt' => 'Created At',
			'updatedAt' => 'Updated At',
		];
	}

	/**
	 * Gets query for [[NewsComments]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getNewsComments()
	{
		return $this->hasMany(NewsComment::className(), ['newsId' => 'id']);
	}
}
