<?php

namespace app\commands;

use app\components\UserRule;
use Yii;
use yii\console\Controller;

class AppController extends Controller
{
	/**
	 * Инициализация приложения.
	 */
	public function actionInit()
	{
		$auth = Yii::$app->authManager;

		$rule = new UserRule();
		$auth->add($rule);

		$user = $auth->createRole('user');
		$user->ruleName = $rule->name;
		$user->description = 'Пользователь';
		$auth->add($user);

		$admin = $auth->createRole('admin');
		$admin->ruleName = $rule->name;
		$admin->description = 'Администратор';
		$auth->add($admin);
		$auth->addChild($admin, $user);
	}
}