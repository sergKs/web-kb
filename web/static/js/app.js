let users = [
	{
		name: "Иван Иванов",
		email: "ivan@mail.ru",
		age: 22
	},
	{
		name: "Дмитрий Дмитриев",
		email: "dima@mail.ru",
		age: 18
	},
	{
		name: "Саша Сергеев",
		email: "alex@mail.ru",
		age: 20
	}
];

/**
 * Фильтрует массив. Возвращает его.
 *
 * @param users
 * @param age
 * @returns {[]}
 */
function filter(users, age = 18) {
	let result = [];

	users.forEach(function (item) {
		if (item.age > age) {
			result.push(item);
		}
	});

	return result;
}

let btn = document.querySelector('.btn-click');
btn.addEventListener('click', function () {
	let textNode = document.querySelector('.title-text');
	let input = document.querySelector('.edit-text');
	textNode.innerHTML = 'Hello World!';
	input.classList.toggle('hidden');
});

let input = document.querySelector('.edit-text');
input.addEventListener('keydown', function (event) {
	let textNode = document.querySelector('.title-text');
	textNode.innerHTML = this.value;
});


