<?php

namespace app\components;

use Yii;
use yii\rbac\Rule;
use app\models\User;

/**
 * UserRule управляет ролями.
 */
class UserRule extends Rule
{
	/**
	 * @inheritdoc
	 */
	public $name = 'role';

	/**
	 * @inheritdoc
	 */
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			/** @var $user User */
			$user = Yii::$app->user->identity;
			$role = $user->{$this->name};

			switch ($item->name) {
				case 'user' : return $role === 'user';
				case 'admin' : return $role === 'admin';
			}
		}

		return false;
	}
}