<?php

namespace app\modules\v1\controllers;

use app\models\Film;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class FilmController extends ApiController
{
	public $modelClass = 'app\models\Film';

	// GET - получить /api/v1/films/<id>
	// POST - создать /api/v1/films
	// PUT - изменить /api/v1/films/<id>
	// DELETE - удалить /api/v1/films/<id>
}