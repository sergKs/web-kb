<?php

namespace app\controllers;

use app\models\News;
use app\models\NewsComment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{
	public function actionIndex($categoryId)
	{
		$news = News::find()
			->where(['categoryId' => $categoryId])
			->orderBy(['createdAt' => SORT_DESC])
			->all();

		return $this->render('index', [
			'news' => $news
		]);
	}

	public function actionView($id)
	{
		$model = News::findOne($id);
		$comments = NewsComment::find()
			->where(['newsId' => $id])
			->all();

		if ($model === null) {
			throw new NotFoundHttpException('Новость не найдена.');
		}

		return $this->render('view', [
			'model' => $model,
			'comments' => $comments
		]);
	}
}
