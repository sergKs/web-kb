<?php

use yii\helpers\Html;

/* @var $news \app\models\News[] */

?>

<?php foreach ($news as $item): ?>

<article>
	<h1><?= Html::a($item->title, ['news/view', 'id' => $item->id]) ?></h1>
</article>

<?php endforeach; ?>
