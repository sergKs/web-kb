<?php

/* @var $model \app\models\News */
/* @var $comments \app\models\NewsComment[] */

?>

<article>
	<h1><?= $model->title ?></h1>
	<p><?= $model->id ?></p>
	<ul>
		<?php foreach ($comments as $comment): ?>
			<li>
				<?= $comment->text ?>
				<time class="small text-muted"><?= date('d.m.Y H:i:s', strtotime($comment->createdAt)) ?></time>
			</li>
		<?php endforeach; ?>
	</ul>
</article>
